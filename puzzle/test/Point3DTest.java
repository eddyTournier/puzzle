import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class Point3DTest {

	protected Point3D pt;
	protected Point3D ptdef;
	
	static int DEFLAUT_X = 3;
	static int DEFLAUT_Y = 1;
	static int DEFLAUT_Z = 4;

	@Before
	public void setUp() throws Exception {
		pt = new Point3D(DEFLAUT_X, DEFLAUT_Y, DEFLAUT_Z);
		ptdef = new Point3D();

	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	
	public void contructeurDefautTest() throws Exception {

		assertEquals( 0, ptdef.getx() );
		assertEquals( 0, ptdef.gety() );
		assertEquals( 0, ptdef.getz() );

	}
	
	@Test
	public void getxTest() throws Exception {
		assertEquals( DEFLAUT_X, pt.getx() );
	}
	

	@Test
	public void getyTest() throws Exception {
		assertEquals( DEFLAUT_Y, pt.gety() );
	}
	
	
	
	@Test
	public void getzTest() throws Exception {
		assertEquals( DEFLAUT_Z, pt.getz() );
	}
	
	
	
	@Test
	public void rotaOnZTest() throws Exception {
		
		pt = new Point3D(DEFLAUT_X, DEFLAUT_Y, DEFLAUT_Z);

		pt.rotaOnZ();
		
		 
		int newx = -DEFLAUT_Y;
		int newy = DEFLAUT_X;
		int newz = DEFLAUT_Z;

		assertEquals( newx, pt.getx() );
		assertEquals( newy, pt.gety() );
		assertEquals( newz , pt.getz() );
		
	}
	
	
	@Test
	public void rotaOnZTestdef() throws Exception {
		ptdef.rotaOnZ();
		assertEquals( 0, ptdef.getx() );
		assertEquals( 0, ptdef.gety() );
		assertEquals( 0, ptdef.getz() );
	}
	
	
	
	@Test
	public void rotaOnXTest() throws Exception {		
		pt.rotaOnX();
		
		assertEquals( 3, pt.getx() );
		assertEquals( -4, pt.gety() );
		assertEquals( 1, pt.getz() );
		
	}
	

	@Test
	public void rotaOnXTestdef() throws Exception {
		ptdef.rotaOnX();
		assertEquals( 0, ptdef.getx() );
		assertEquals( 0, ptdef.gety() );
		assertEquals( 0, ptdef.getz() );
	}
	
	
	@Test
	public void rotaOnYTest() throws Exception {		
		pt.rotaOnY();
		assertEquals( 1, pt.gety() );
		assertEquals( 4, pt.getx() );
		assertEquals( -3, pt.getz() );
	}

	
	@Test
	public void rotaOnYTestdef() throws Exception {
		ptdef.rotaOnY();
		assertEquals( 0, ptdef.getx() );
		assertEquals( 0, ptdef.gety() );
		assertEquals( 0, ptdef.getz() );
	}

	
	@Test
	public void toStringTest() throws Exception {		
		assertEquals( "( x:3, y:1, z:4 )", pt.toString() );
		assertEquals( "( x:0, y:0, z:0 )", ptdef.toString() );

	}



}
