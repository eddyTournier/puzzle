import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;

public class Shape3DTest {

	public static Shape3D shapedefaut;
	public static Shape3D shape;
	private ArrayList<Point3D> listnewpt;
	
	
	static int DEFLAUT_X = 3;
	static int DEFLAUT_Y = 1;
	static int DEFLAUT_Z = 4;
	
	
	
	static int PT_1_X = DEFLAUT_X+3;
	static int PT_1_Y =  DEFLAUT_Y+1;
	static int PT_1_Z = DEFLAUT_Z;
	
	

	static int PT_2_X = DEFLAUT_X+1;
	static int PT_2_Y = DEFLAUT_Y+2;
	static int PT_2_Z = DEFLAUT_Z+5;
	
	
	
	static String DEFLAUT_NAME = "shape";

	
	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
			shapedefaut = new Shape3D();
			
			shape = new Shape3D(DEFLAUT_X, DEFLAUT_Y, DEFLAUT_Z, DEFLAUT_NAME );
			shape.pushPoint(PT_1_X, PT_1_Y, PT_1_Z);

			
			Point3D pt = new Point3D( PT_2_X, PT_2_Y, PT_2_Z);
			shape.pushPoint(pt);

	}

	@Test
	public void contructeurDefautTest() throws Exception {
		
		assertEquals( 0, shapedefaut.getlistPt().get(0).getx() );
		assertEquals( 0, shapedefaut.getlistPt().get(0).gety() );
		assertEquals( 0, shapedefaut.getlistPt().get(0).getz() );
		assertEquals( 1, shapedefaut.getID() );
		assertEquals( 1, shapedefaut.getID() );

		assertEquals( 2, Shape3D.getNumberOfCube() );


	}
	
	@Test
	public void contructeurvaleurTest() throws Exception {
		
		assertEquals( DEFLAUT_X, shape.getlistPt().get(0).getx() );
		assertEquals( DEFLAUT_Y, shape.getlistPt().get(0).gety() );
		assertEquals( DEFLAUT_Z, shape.getlistPt().get(0).getz() );
		assertEquals( 2, shape.getID() );
		assertEquals( DEFLAUT_NAME, shape.getName() );
		
		assertEquals( 2, Shape3D.getNumberOfCube() );



	}
	
	
	@Test
	public void ajoutPTTest() throws Exception {
		
		assertEquals( DEFLAUT_X, shape.getlistPt().get(0).getx() );
		assertEquals( DEFLAUT_Y, shape.getlistPt().get(0).gety() );
		assertEquals( DEFLAUT_Z, shape.getlistPt().get(0).getz() );
		
		

		assertEquals( PT_1_X, shape.getlistPt().get(1).getx() );
		assertEquals( PT_1_Y, shape.getlistPt().get(1).gety() );
		assertEquals( PT_1_Z, shape.getlistPt().get(1).getz() );
		
		
		assertEquals( PT_2_X, shape.getlistPt().get(2).getx() );
		assertEquals( PT_2_Y, shape.getlistPt().get(2).gety() );
		assertEquals( PT_2_Z, shape.getlistPt().get(2).getz() );
		
		assertEquals( 2, shape.getID() );
		assertEquals( DEFLAUT_NAME, shape.getName() );


	}
	
	

	
	@Test
	public void rotaShapeOnZTest() throws Exception {
		
		listnewpt = new ArrayList<Point3D>();
		
		for (int i = 0; i < shape.getlistPt().size(); i++) {
			
			int newx = -shape.getlistPt().get(i).gety();
			int newy = shape.getlistPt().get(i).getx();
			int newz = shape.getlistPt().get(i).getz();
			
			listnewpt.add(new Point3D(newx, newy, newz ));
			
		}
		
		
		
		
		shape.rotaShapeOnZ();
		shapedefaut.rotaShapeOnZ();
		
		for (int i = 0; i < listnewpt.size(); i++) {

		assertEquals( listnewpt.get(i).getx(), shape.getlistPt().get(i).getx() );
		assertEquals( listnewpt.get(i).gety(), shape.getlistPt().get(i).gety() );
		assertEquals( listnewpt.get(i).getz(), shape.getlistPt().get(i).getz() );
		
		}
		
		assertEquals( 0, shapedefaut.getlistPt().get(0).getx() );
		assertEquals( 0, shapedefaut.getlistPt().get(0).gety() );
		assertEquals( 0, shapedefaut.getlistPt().get(0).getz() );
		

	}
	
	
	


	@Test
	public void rotaShapeOnXTest() throws Exception {
		
		listnewpt = new ArrayList<Point3D>();
		
		for (int i = 0; i < shape.getlistPt().size(); i++) {
			
			int newx = shape.getlistPt().get(i).getx();
			int newy = -shape.getlistPt().get(i).getz();
			int newz = shape.getlistPt().get(i).gety();
			
			listnewpt.add(new Point3D(newx, newy, newz ));
			
		}
		
		
		
		
		shape.rotaShapeOnX();
		shapedefaut.rotaShapeOnX();
		
		for (int i = 0; i < listnewpt.size(); i++) {

		assertEquals( listnewpt.get(i).getx(), shape.getlistPt().get(i).getx() );
		assertEquals( listnewpt.get(i).gety(), shape.getlistPt().get(i).gety() );
		assertEquals( listnewpt.get(i).getz(), shape.getlistPt().get(i).getz() );
		
		}
		
		assertEquals( 0, shapedefaut.getlistPt().get(0).getx() );
		assertEquals( 0, shapedefaut.getlistPt().get(0).gety() );
		assertEquals( 0, shapedefaut.getlistPt().get(0).getz() );
		

	}
	
	
	

	
	@Test
	public void rotaShapeOnYTest() throws Exception {
		
		listnewpt = new ArrayList<Point3D>();
		
		for (int i = 0; i < shape.getlistPt().size(); i++) {
			
			int newx = shape.getlistPt().get(i).getz();
			int newy = shape.getlistPt().get(i).gety();
			int newz = -shape.getlistPt().get(i).getx();
			
			listnewpt.add(new Point3D(newx, newy, newz ));
			
		}
		
		
		
		
		shape.rotaShapeOnY();
		shapedefaut.rotaShapeOnY();
		
		for (int i = 0; i < listnewpt.size(); i++) {

		assertEquals( listnewpt.get(i).getx(), shape.getlistPt().get(i).getx() );
		assertEquals( listnewpt.get(i).gety(), shape.getlistPt().get(i).gety() );
		assertEquals( listnewpt.get(i).getz(), shape.getlistPt().get(i).getz() );
		
		}
		
		assertEquals( 0, shapedefaut.getlistPt().get(0).getx() );
		assertEquals( 0, shapedefaut.getlistPt().get(0).gety() );
		assertEquals( 0, shapedefaut.getlistPt().get(0).getz() );
		

	}
	
	
	
	

	
	@Test
	public void transShapeOnXTest() throws Exception {
		
		listnewpt = new ArrayList<Point3D>();
		
		for (int i = 0; i < shape.getlistPt().size(); i++) {
			
			int shapeNewX = shape.getlistPt().get(i).getx() + 1;
			int shapeNewY = shape.getlistPt().get(i).gety();
			int shapeNewZ = shape.getlistPt().get(i).getz();
			
			listnewpt.add(new Point3D(shapeNewX, shapeNewY, shapeNewZ ));
			
		}
		
		
		
		int shapedefautNewX = shapedefaut.getlistPt().get(0).getx() + 1;
		int shapedefautNewY = shapedefaut.getlistPt().get(0).gety();
		int shapedefautNewZ = shapedefaut.getlistPt().get(0).getz();
		
		
		shape.transShapeOnX();
		shapedefaut.transShapeOnX();
		
		for (int i = 0; i < listnewpt.size(); i++) {

		assertEquals( listnewpt.get(i).getx(), shape.getlistPt().get(i).getx() );
		assertEquals( listnewpt.get(i).gety(), shape.getlistPt().get(i).gety() );
		assertEquals( listnewpt.get(i).getz(), shape.getlistPt().get(i).getz() );
		
		}
		
		assertEquals( shapedefautNewX, shapedefaut.getlistPt().get(0).getx() );
		assertEquals( shapedefautNewY, shapedefaut.getlistPt().get(0).gety() );
		assertEquals( shapedefautNewZ, shapedefaut.getlistPt().get(0).getz() );
		

	}
	
	
	
	
	

	@Test
	public void sharetostringtest() throws Exception {
	

		
		Shape3D shapeStringtest = new Shape3D( 1,2,3 );
		shapeStringtest.pushPoint( 0,0,0 );
		shapeStringtest.pushPoint( 4,5,6);

		String chainestring = "[( x:1, y:2, z:3 ) ; ( x:0, y:0, z:0 ) ; ( x:4, y:5, z:6 )]";
		
	
		assertEquals( chainestring, shapeStringtest.toString() );

		
	}
	
	
	
	
	

}
