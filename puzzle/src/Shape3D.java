import java.util.ArrayList;




public class Shape3D {

	/**
	 * description de la class Shape3D
	 * elle permet de creer une forme "shape" fonction de son 1er point et de le manipuler sur 3 plans x,y,z 
	 * 
	 *
	 * @author : Eddy
	 * 
	 */
	
	
	private static int NumberOfCube = 0;
    private int id;
    private String name;
	private ArrayList<Point3D> listPt;




	
	
	/**
	 * contructeur d'un objet forme avec son 1er point et son nom 
	 *	la piece aura un identifiant unique
	 *
	 * @param x 
	 * 		type integer place le 1er point de la forme sur l'axe des x
	 * 
	 * @param y
	 * 	 	type integer place le 1er point de la forme sur l'axe des y
	 *
	 * @param z
	 * 	 	type integer place le 1er point de la forme sur l'axe des z
	 * 
	 * @param name
	 * 	 	type string la forme  peut avoir un nom sour forme de chaine de caractere
	 * 
	 */
	
	
	
	public Shape3D( int x, int y, int z, String name) {
		
		this.id = ++NumberOfCube;
		
		this.setName(name);
		
		listPt = new ArrayList<Point3D>();
		
		this.pushPoint( new Point3D(x,y,z) );
		
	}
	
	

	
	/**
	 * contructeur avec les coordonnees du 1er point de la piece
	 * la piece aura un identifiant unique
	 *
	 * @param x 
	 * 		type integer place le 1er point de la forme sur l'axe des x
	 * 
	 * @param y
	 * 	 	type integer place le 1er point de la forme sur l'axe des y
	 *
	 * @param z
	 * 	 	type integer place le 1er point de la forme sur l'axe des z
	 * 
	 * 
	 * 
	 */
	
	public Shape3D( int x, int y, int z) {
		this(x, y, z, null);
	}
	
	
	/**
	 * contructeur avec juste un nom
	 * les coordonnees du 1er point sont initialse a (0,0,0)
	 * la piece aura un identifiant unique
	 * 
	 * @param name
	 * 	 	type string le cube peut avoir un nom sour forme de chaine de caractere
	 * 
	 */
	
	public Shape3D(String name) {
		this(0, 0, 0, name);
	}
	
	
	/**
	 * 
	 * Le contructeur par defaut d'un objet shape
	 * les coordonnees du 1er point sont initialse a (0,0,0)
	 * la piece aura un identifiant unique
	 * cette objet shape n'aura pas de nom dedie
	 * 
	 */
	
	public Shape3D() {
		this(0, 0, 0, null);
	}
	
	
	/**
	 * methode qui retourne tout les point de l'objet courant shape sous forme d'une ArrayList 
	 * 
	 */
	
	
	public  ArrayList<Point3D> getlistPt(){
		return this.listPt;

	}
	


	/**
	 * methode qui retourne l'indentifiant de l'objet courant shape
	 * 
	 */
	
	public int getID() {
        return this.id;
    }
	
	
	
	/**
	 * methode qui retourne le nombre totale d'objet shape qui a été crée
	 * 
	 */
	
	public static int getNumberOfCube() {
	    return NumberOfCube;
	}

	
	

	/**
	 * methode qui retourne le nom de la piece sous forme de chaine de caractere
	 * 
	 */
	
	public String getName() {
		return this.name;
	}

	
	/**
	 * methode qui attribut un nom a la piece
	 * par defaut le nom de la piece sera composé de l'identifiant precede d'une phrase : "Piece  numero "
	 * 
	 *	@param name
	 * 	 	type string le cube peut avoir un nom sour forme de chaine de caractere
	 */
	
	public void setName(String name) {
		
		if(name == null){
			name = "Piece numero " + getID();
		}
		
		this.name = name;
		
	}
	
	
	
	/**
	 * methode qui ajout a la piece avec un objet point a l'objet shape courant
	 *	 
	 * @param pt
	 *    type Point3D
	 *
	 */
	
	public void pushPoint(Point3D pt) {
		this.getlistPt().add(pt);
	}
	

	/**
	 * methode qui ajout a la piece avec les coordonnees point
	 * 
	 * @param x 
	 * 		type integer place  le nouveau point sur l'axe des x
	 * 
	 * @param y
	 * 	 	type integer place le nouveau point sur l'axe des y
	 *
	 * @param z
	 * 	 	type integer place le nouveau point sur l'axe des z
	 * 
	 * */
	
	public void pushPoint(int x, int y, int z ) {
		this.pushPoint(new Point3D(x,y,z) );
	}

	
	/**
	 * methode qui sert a la rotation de la forme sur l'axe des x
	 * 
	 **/
	
	
	public void rotaShapeOnX(){
		for (int i = 0; i < this.getlistPt().size(); i++) {
			this.getlistPt().get(i).rotaOnX();
		}
		
	}
	
	
	
	/**
	 * methode qui sert a la rotation de la forme sur l'axe des y
	 * 
	 **/
	
	
	public void rotaShapeOnY(){
		for (int i = 0; i < this.getlistPt().size(); i++) {
			this.getlistPt().get(i).rotaOnY();
		}
		
	}
	
	
	
	
	/**
	 * methode qui sert a la rotation de la forme sur l'axe des z
	 * 
	 **/
	
	
	public void rotaShapeOnZ(){
		for (int i = 0; i < this.getlistPt().size(); i++) {
			this.getlistPt().get(i).rotaOnZ();	
		}		
	}

	
	/**
	 * methode qui sert a la translation de la forme sur l'axe des x
	 * par defaut avec un delta de 1
	 * 
	 **/
	
	
	public void transShapeOnX(){
		this.transShapeOnX(1);	
	}
	
	/**
	 * methode qui sert a la translation de la forme sur l'axe des x
	 * d'une valeur deltaX
	 * 
	 * @param deltaX
	 * 	type integer delta de x
	 * 
	 **/
	
	
	public void transShapeOnX(int deltaX){
		for (int i = 0; i < this.getlistPt().size(); i++) {
			this.getlistPt().get(i).transx(deltaX);	
		}		
	}
	
	
	
	
	/**
	 * methode qui sert a la translation de la forme sur l'axe des Y
	 * par defaut avec un delta de 1
	 * 
	 **/
	
	public void transShapeOnY(){
		this.transShapeOnY(1);	
	}
	
	/**
	 * methode qui sert a la translation de la forme sur l'axe des Y
	 * d'une valeur deltaZ
	 * 
	 * @param deltaY
	 * 	type integer delta de y
	 * 
	 **/
	
	public void transShapeOnY(int deltaY){
		for (int i = 0; i < this.getlistPt().size(); i++) {
			this.getlistPt().get(i).transx(deltaY);	
		}		
	}
	
	
	/**
	 * methode qui sert a la translation de la forme sur l'axe des Z
	 * par defaut avec un delta de 1
	 * 
	 **/
	
	public void transShapeOnZ(){
		this.transShapeOnZ(1);	
	}
	
	/**
	 * methode qui sert a la translation de la forme sur l'axe des Y
	 * d'une valeur deltaZ
	 * 
	 * @param deltaZ
	 * 	type integer delta de Z
	 * 
	 **/
	
	public void transShapeOnZ(int deltaZ){
		for (int i = 0; i < this.getlistPt().size(); i++) {
			this.getlistPt().get(i).transx(deltaZ);	
		}		
	}
	
	
	
	public String toString(){
		
		String coordonneeString = "";
		
		for (int i = 0; i < this.getlistPt().size(); i++) {
			if(i != 0){
				coordonneeString += " ; ";
			}
			
			coordonneeString += this.getlistPt().get(i).toString();	
		}	
		
		coordonneeString = "[" + coordonneeString + "]";
		
		return coordonneeString;
	}
	
	
}