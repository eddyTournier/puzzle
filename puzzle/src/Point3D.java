public class Point3D {
	
	/**
	 * description de la class Point3D
	 * elle permet de creer un point et de le manipuler sur 3 plans x,y,z 
	 * 
	 *
	 * @author : Eddy
	 * 
	 */
	
	
	private int x;
	private int y;
	private int z;
	
	/**
	 * contructeur avec des parametres particulier et a preciser
	 * @param x 
	 * 		type integer place le point sur l'axe des x
	 * 
	 * @param y
	 * 	 	type integer place le point sur l'axe des y
	 *
	 * @param z
	 * 	 	type integer place le point sur l'axe des z
	 * 
	 */
	
	
	public Point3D(int x, int y, int z  ) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	/**
	 * constructeur par defaut x=0, y=0, z=0
	 */
	
	public Point3D() {
		this(0, 0, 0);
	}
	
	
	/**
	 * @return la valeur actuel de x
	 */
	
	
	public int getx(){
		return this.x;
	}
	
	/**
	 * @return  la valeur actuel de y
	 */
	
	public int gety(){
		return this.y;
	}

	
	/**
	 * @return  la valeur actuel de z
	 */
	
	public int getz(){
		return this.z;
	}
	
	
	
	/**
	 * methode modificateur le l'attribut x
	 * @param x
	 */
	
	
	public void setx(int x){
		 this.x = x;
	}
	
	/**
	 * methode modificateur le l'attribut y
	 * @param y
	 */
	
	public void sety(int y){
		 this.y = y;
	}

	
	/**
	 * methode modificateur le l'attribut z
	 * @param z
	 */
	
	
	public void setz(int z){
		 this.z = z;
	}
	
	/**
	 * translation du point sur x en ajoutant la valeur de deltaX
	 * @param deltaX
	 */
	
	public void transx(int deltaX){
		setx(getx() + deltaX);
	}
	
	/**
	 * translation par defaut du point sur x en ajoutant la valeur 1
	 */
	
	public void transx(){
		transx(1);
	}
	
	
	/**
	 * translation du point sur y en ajoutant la valeur de deltaY
	 * @param deltaY
	 */
	
	public void transy(int deltaY){
		sety(gety() + deltaY);
	}

	
	/**
	 * translation par defaut du point sur y en ajoutant la valeur 1
	 */
	
	public void transy(){
		transy(1);
	}
	
	
	/**
	 * translation par defaut du point sur z en ajoutant la valeur 1
	 * @param deltaZ
	 */
	
	public void transz(int deltaZ){
		setz(getz() + deltaZ);
	}
	
	
	/**
	 * translation par defaut du point sur z en ajoutant la valeur 1
	 */
	
	
	public void transz(){
		transz(1);
	}
	
	
	/**
	 * rotation du point de 90degres par rapport a l'axe de X
	 */
	
	public void rotaOnX(){
		
		int tempsz;
		tempsz = getz();
		
		setz(gety());
		sety(-tempsz);
		
	}
	
	

	/**
	 * rotation du point de 90degres par rapport a l'axe de Y
	 */
	
	public void rotaOnY(){

		int tempsx;
		tempsx = getx();
		
		setx(getz());
		setz(-tempsx);
		
	}
	
	

	/**
	 * rotation du point de 90degres par rapport a l'axe de Z
	 */
	
	public void rotaOnZ(){
	
		int tempsy;
		tempsy = gety();
		
		sety(getx());
		setx(-tempsy);
		
	}
	
	
/**
 * sert a me retourner mes coordonnees de mon point avec une chaine de caractere
 * 
 */
	
	public String toString(){
		return "( x:"+ getx() +", y:"+ gety() + ", z:"+ getz() +" )";
	}
	 
	
	
	
	
}
